package com.vk.reactorCoreTests.publishers;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Arrays;

public class FluxTest {

    @Test
    public void testSimpleFlux() {
        Flux.just("One", "Two", "Three")
                .log()
                .subscribe();
    }

    @Test
    public void flux_whenValueIsArray_returnOneOnNextWithList() {
        Flux.just(Arrays.asList("One", "Two", "Three"))
                .log()
                .subscribe();
    }

    @Test
    public void flux_fromIterable_returnMultipleOnNext() {
        Flux.fromIterable(Arrays.asList("One", "Two", "Three"))
                .log()
                .subscribe();

    }

    @Test
    public void flux_fromRange_returnValuesInRange() {
        Flux.range(14, 10)
                .log()
                .subscribe();
    }

    @Test
    public void flux_fromDuaration_returnValuesInDuaration() throws InterruptedException {
        Flux.interval(Duration.ofSeconds(1))
                .log()
                .subscribe();
        Thread.sleep(5000);
    }
}
