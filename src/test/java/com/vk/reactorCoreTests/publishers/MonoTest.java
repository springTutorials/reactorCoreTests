package com.vk.reactorCoreTests.publishers;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;

public class MonoTest {

    @Test
    public void monoNotSubscribe_monoInactive_noLogs() {
        Mono.just("Hello Mono").log();
    }

    @Test
    public void monoSubscribed_monoCompleted_printWorkFlow() {
        Mono.just("Hello Mono")
                .log()
                .subscribe();
    }

    @Test
    public void monoSubscribed_monoError_printWorkFlow() {
        Mono.error(new Exception("This is error message"))
                .log()
                .subscribe();
    }

    @Test
    public void monoSubscribed_monoOnEachStep_printWorkFlow() {
        Mono.just("Hello Mono")
                .log()
                .doOnSubscribe(c -> System.out.println("print on subscribe"))
                .doOnRequest(c -> System.out.println("print on request"))
                .doOnNext(c -> System.out.println("print on next"))
                .doOnSuccess(c -> System.out.println("print on success"))
                .subscribe();
    }

    @Test
    public void monoSubscribed_monoOnEachStepWhenError_printWorkFlowWithError() {
        Mono.error(new Exception())
                .log()
                .doOnSubscribe(c -> System.out.println("print on subscribe"))
                .doOnRequest(c -> System.out.println("print on request"))
                .doOnNext(c -> System.out.println("print on next"))
                .doOnError(c -> System.out.println("print on error"))
                .subscribe();
    }

}