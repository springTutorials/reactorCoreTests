package com.vk.reactorCoreTests.publishers;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

public class OperatorTest
{
	@Test
	public void map_mapMultiply10_printValuesMultipliedBy10() {
		Flux.range(0, 5)
				.log()
				.map(a -> a *10)
				.subscribe(System.out::println);
	}

	@Test
	public void flatMap_flatMapMultiply10_printValuesMultipliedBy10() {
		Flux.range(0, 5)
				.log()
				.flatMap(a -> Flux.range(a * 10, 2))
				.subscribe(System.out::println);
	}

	@Test
	public void flatMapMany_convertMonoToFlux_printValuesFrom1To3() {
		//flatMapMany operator used to convert Mono to Flux
		Mono.just(3)
				.log()
				.flatMapMany(i -> Flux.range(1, i))
				.subscribe(System.out::println);
	}

	@Test
	public void concat_concatTwoFluxValues_concatValuesPrinted() throws InterruptedException {
		Flux<Integer> oneToFive = Flux.range(1, 5)
				.delayElements(Duration.ofMillis(200));
		Flux<Integer> sixToTen = Flux.range(6, 5)
				.delayElements(Duration.ofMillis(400));

		Flux.concat(oneToFive, sixToTen)
				.log()
				.subscribe(System.out::println);

		Thread.sleep(4000);
	}

	@Test
	public void merge_mergeTwoFluxValues_mergedValuesIsPrinted() throws InterruptedException {
		Flux<Integer> oneToFive = Flux.range(1, 5)
				.delayElements(Duration.ofMillis(200));
		Flux<Integer> sixToTen = Flux.range(6, 5)
				.delayElements(Duration.ofMillis(400));

		Flux.merge(oneToFive, sixToTen)
				.log()
				.subscribe(System.out::println);

		Thread.sleep(4000);
	}

	@Test
	public void zip_zipTwoFluxValues_zippedValuesIsPrinted() throws InterruptedException {
		Flux<Integer> oneToFive = Flux.range(1, 5)
				.delayElements(Duration.ofMillis(200));
		Flux<Integer> sixToTen = Flux.range(6, 5)
				.delayElements(Duration.ofMillis(400));

		Flux.zip(oneToFive, sixToTen, (i1, i2) -> i1 + ", " + i2)
				.log()
				.subscribe(System.out::println);

		Thread.sleep(4000);
	}
}
